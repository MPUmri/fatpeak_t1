function data = ir_signal_invf(invtimes, t1, s0, f, tr)
% data = ir_signal_trinf(times, t1, s0, f)
%   tr is ignored, assumed infinite

data = s0*(1-(1-f)*exp(-invtimes/t1));