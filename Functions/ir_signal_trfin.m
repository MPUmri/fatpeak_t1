function data = ir_signal_trfin(invtimes, t1, s0, f, tr)
% data = ir_signal_trfin(mytimes, t1, s0, f, tr)
% data = s0*(1 - (1-cos(f))*exp(-mytimes/t1) - cos(f)*exp(-tr/t1) );

%data = s0*(1 - (1-cos(f))*exp(-mytimes/t1) - cos(f)*exp(-tr/t1) );
data = s0*(1 - (1-f)*exp(-invtimes/t1) - f*exp(-tr/t1) );
