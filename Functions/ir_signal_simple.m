function data = ir_signal_simple(invtimes, t1, s0, f, tr)
% data = ir_signal_simple(times, t1, s0, f, tr)

data = s0*(1 - 2*exp(-invtimes/t1));