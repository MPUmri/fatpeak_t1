function data = ir_signal_rutt(invtimes, t1, s0, f, tr)
% data = ir_signal_rutt(times, t1, s0, f, tr)

data = s0*(1 - 2*f*exp(-invtimes/t1) + exp(-tr/t1));