function [t1, s0, inv_fraction, ssq_res, exit_flag, maxfunevals] = fit_inv_recov(signed_data, inv_times, tr, guess, eq_flag)
%
% [t1, s0, inv_fraction, ssq_res, exit_flag, maxfunevals] = fit_inv_recov(signed_data, inv_times, tr, guess, eq_flag)
%
%   Function to fit inversion recovery data and determine observed T1, signal at TI = 0, and inversion fraction
%
%	eq_flag: determines which model equation to use
%		1 = IR signal equation with term for imperfect inversion (assumes TR = Inf)
%		2 = IR signal equation with terms for imperfect inversion and for finite TR
%		3 = different version of eq. 2 with terms for imperfect inversion and for finite TR (unstable, for testing only)
%		4 = simple IR signal equation (assumes perfect inversion and TR = Inf) (default choice)
%		
%   Note that the acquisition-dependent signal decay due to T2 (SE) or T2-star (GRE) simply gets wrapped into s0
%
%
%   INPUT
%	signed_data: signed recover data (magnitude and phase data combined)
%	inv_times: vector containing inversion times used in experiment
%   tr: repetition time
%	guess: vector containing guesses for [ t1, s0, f ]
%
%--------------------------------------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------------------------------------
%
%   Ives Levesque, Nov 2006
%
%   modifications
%
%   2011/11/10
%   incorporated a more complete signal equation with TR as paramter
%   2012/07/19
%   added an "until" loop to increase MaxFunEvals when fit fails...  needs testing
%   2013/02/06
%   added default signal equation option
%--------------------------------------------------------------------------------------------------------------------

if eq_flag == 1
    signal_eq = 'ir_signal_invf';
elseif eq_flag == 2
    signal_eq = 'ir_signal_trfin';
elseif eq_flag == 3
    signal_eq = 'ir_signal_rutt';
elseif eq_flag == 4
    signal_eq = 'ir_signal_simple';
elseif ~exist('eq_flag')
    % default to simple IR
    signal_eq = 'ir_signal_simple';
end

[result, ssq_res, exit_flag] = fminsearch(@err_fit_2, guess, [], signed_data, inv_times, tr, signal_eq);
myoptions = optimset('fminsearch');
oldMaxFunEvals = 200*length(guess);
oldMaxIter = 200*length(guess);
count = 0;
if exit_flag ~= 1
    disp('more evals')
end
while exit_flag ~= 1 & count < 5 ;
    newMaxFunEvals = oldMaxFunEvals*4;
    newMaxIter = oldMaxIter*4;
    myoptions = optimset(myoptions,'MaxFunEvals', newMaxFunEvals,'MaxIter',newMaxIter);
    [result, ssq_res, exit_flag] = fminsearch(@err_fit_2, guess, myoptions, signed_data, inv_times, tr, signal_eq);
    oldMaxFunEvals = newMaxFunEvals;
    count=count+1;
    %exit_flag
end


%count

t1 = result(1);
s0 = result(2);
inv_fraction = result(3);
maxfunevals = oldMaxFunEvals;

%-------------------------------------------------------------------------------
function err = err_fit_2(params, sample, invtimes, tr, myeq)

%t1 = params(1);
%s0 = params(2);
%f = params(3);

% return sum of squares residual

err = sum((sample - feval(myeq, invtimes, params(1), params(2), params(3), tr)).^2);



%-------------------------------------------------------------------------------
%   EXPORTED
%function data = ir_signal_trinf(times, t1, s0, f, tr)
%
%data = s0*(1-(1-f)*exp(-times/t1));


%-------------------------------------------------------------------------------
%   EXPORTED
%function data = ir_signal_trfin(times, t1, s0, f, tr)
%
%data = s0*(1 - (1-f)*exp(-times/t1) + f*exp(-times/tr) );


%-------------------------------------------------------------------------------
%   EXPORTED
% function data = ir_signal_rutt(times, t1, s0, f, tr)
% 
% data = s0*(1 - 2*f*exp(-times/t1) + exp(-tr/t1) );