%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Veronique Fortier 2021 - MIT License
%
% Non linear least square fit for T1 measurements with STEAMMRS TI series                                           
%
% Inputs                                                                 
%   1. Inversion times 
%        -TI: Row vector, ms unit
%   2. Peak area for each TI measurement
%        - relAmplitude: 2D matrix, lines corresponding to the different TI
%        measurements, and column to spectral peaks. The last column should
%        corresponds to the water peak.
%   3. Peak spectral location for each TI measurement
%        - freqPeak: Vector of chemical shift in ppm unit, last value
%        should correspond to the water peak
%
% Outputs
%   1. T1 values for each peak
%        - T1_TI: Vector, last value corresponds to the water peak
%   2. Coeffficient of determination for the non linear fit
%        - r_squared: Vector, last value corresponds to the water peak
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function [ t1_final, r_squared_final ] = STEAM_TI_T1( TI, relAmplitude, freqPeaks,tr)

    %	eq_flag: determines which model equation to use for T1 fit
        % 1 = IR signal equation with term for imperfect inversion (assumes TR = Inf)
        % 2 = IR signal equation with terms for imperfect inversion and for finite TR
        % 3 = different version of eq. 2 with terms for imperfect inversion and for finite TR (unstable, for testing only)
        % 4 = simple IR signal equation (assumes perfect inversion and TR = Inf) (default choice)

    eq_flag=2; % choose equation 
    

    for TI_index=1:length(TI)
        magn_matrix(TI_index,:)=relAmplitude(TI_index,:);
    end

    clear complex_matrix1 complex_matrix2
    for index=1:size(relAmplitude,2)
        [value,min_index]=min(magn_matrix(:,index));
        complex_matrix1(1:(min_index-1),index)=magn_matrix(1:(min_index-1),index)*-1;
        complex_matrix1(min_index,index)=magn_matrix(min_index,index)*-1;
        complex_matrix1((min_index+1):length(TI),index)=magn_matrix((min_index+1):end,index);

        complex_matrix2(1:(min_index-1),index)=magn_matrix(1:(min_index-1),index)*-1;
        complex_matrix2(min_index,index)=magn_matrix(min_index,index);
        complex_matrix2((min_index+1):length(TI),index)=magn_matrix((min_index+1):end,index);
    end

    % Initial guess for the three free parameters for the T1 fit (fat T1, S0, f)
    guess=[300, 0.01, -1];

    for index=1:size(relAmplitude,2)
        data(:,index)=complex_matrix1(:,index);
        signed_data=data(:,index)';
        data2(:,index)=complex_matrix2(:,index);
        signed_data2=data2(:,index)';

        if index==9
            guess=[550, 0.01, -1]; % Initial guess for the three free parameters for the T1 fit (water T1, S0, f)
        end
        [t1(index), s0(index), inv_fraction(index), ssq_res(index), exit_flag(index), maxfunevals(index)] = fit_inv_recov(signed_data, TI, tr, guess, eq_flag);

        SSE=sum((signed_data- s0(index)*(1 - (1-inv_fraction(index))*exp(-TI/t1(index)) + inv_fraction(index)*exp(-tr/t1(index)) )).^2);
        y_mean=mean(signed_data);
        SST=sum((signed_data-y_mean).^2);
        r_squared1(index)=1-SSE/SST;


        [t12(index), s02(index), inv_fraction2(index), ssq_res2(index), exit_flag2(index), maxfunevals2(index)] = fit_inv_recov(signed_data2, TI, tr, guess, eq_flag);
        SSE=sum((signed_data2- s02(index)*(1 - (1-inv_fraction2(index))*exp(-TI/t12(index))  + inv_fraction2(index)*exp(-tr/t12(index)) )).^2);
        y_mean=mean(signed_data2);
        SST=sum((signed_data2-y_mean).^2);
        r_squared2(index)=1-SSE/SST;

    end

    % choose best fit
    for index=1:size(relAmplitude,2)
        if r_squared2(index)>r_squared1(index)
            data_final(:,index)=data2(:,index);
            s0_final(index)=s02(index);
            t1_final(index)=t12(index);
            inv_fraction_final(index)=inv_fraction2(index);
            r_squared_final(index)=r_squared2(index);
        else
            data_final(:,index)=data(:,index);
            s0_final(index)=s0(index);
            t1_final(index)=t1(index);
            inv_fraction_final(index)=inv_fraction(index);
            r_squared_final(index)=r_squared1(index);
        end

    end


    %% plot fit
    xTI=[1:4000];
    for index=1:size(relAmplitude,2)
        figure();hold on;plot(TI,data_final(:,index), ' bo','markersize',8,'markerfacecolor','b')
        plot(xTI,s0_final(index).*(1 - (1-inv_fraction_final(index)).*exp(-xTI./t1_final(index)) - inv_fraction_final(index)*exp(-tr/t1_final(index))),':k','linewidth',2)
        set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');grid on
        xlabel('Inversion time [ms]')
        ylabel('Signal [a.u.]')
        str=sprintf('Peak: %0.1f ppm / T_1=%0.0f ms / R^2=%0.2f',round(freqPeaks(index),1),round(t1_final(index)),round(r_squared_final(index),2));
        legend('Data','Fit')
        title(str);

    end

end

 
