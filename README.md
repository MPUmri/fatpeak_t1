T1 calculations based on STEAM MRS TI series and modelling of T1 variations as a function of fat fraction
Copyright Veronique Fortier 2021 - MIT License 
_______________________________________________


.mat files of T1 values measured in dairy cream and safflower oil emulsion are included in the data folder.


Refer to the publication for more details on the experiments:
V. Fortier and I. R. Levesque, Longitudinal relaxation in fat-water mixtures and its dependence on fat content at 3 T, NMR in Biomed., 2021.

________________________________________________

Raw and processed MRS data used in the publication are available at: https://osf.io/nfkhe/
