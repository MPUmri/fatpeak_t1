%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Veronique Fortier 2021 - MIT License
%
% MRS T1 measurements based on STEAM MRS TM series                                             
%
% Required:                                                                 
%   1. Inversion times 
%        -TI: Row vector, ms unit
%   2. Peak area for each TI measurement (processed and raw MRS data available at 
%      https://osf.io/nfkhe/) :
%        - relAmplitude: 2D matrix, lines corresponding to the different TI
%        measurements, and column to spectral peaks. The last column should
%        corresponds to the water peak.
%   3. Peak spectral location for each TI measurement (processed and raw MRS data 
%      available at 
%      https://osf.io/nfkhe/) :
%        - freqPeaks: Vector of chemical shift in ppm unit, last value
%        should correspond to the water peak
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


filesPath=strcat(pwd,'/Functions');
addpath(genpath(filesPath));

filesPath=strcat(pwd,'/Data');
addpath(genpath(filesPath));

safflowerOilEmulsion=1;
dairyCream=0;

%% Read TI and TR directly for the data files or input the values manually here

TI=[16, 90, 175, 350, 475, 650, 800, 1150, 3200]; 
tr=6000;

%% Load processed data

% To use processed MRS data (safflower oil emulsion), load these mat
% files:
if safflowerOilEmulsion==1
    load('processedMRS_Data_Microlipid.mat');
    relAmplitude=processedMRS_Data_Microlipid.exp1.relAmplitude(:,:,1); %choose the experiment and FF of interest
    freqPeaks=processedMRS_Data_Microlipid.exp1.freqPeaks(:,:,1); %choose the experiment and FF of interest
end

% To use processed MRS data (dairy cream emulsion), load these mat
% files:
if dairyCream==1
    load('processedMRS_Data_DairyCream.mat');
    relAmplitude=processedMRS_Data_DairyCream.exp1.relAmplitude(:,:,1); %choose the experiment and FF of interest
    freqPeaks=processedMRS_Data_DairyCream.exp1.freqPeaks(:,:,1); %choose the experiment and FF of interest
    
end

%% Calculate relative abundance of each fat peak

for index=1:length(TI)
    relAmplitude_fat(index,:)=relAmplitude(index,1:end-1);

    for indexPeak=1:length(freqPeaks)-1
        relAmp_fat(index,indexPeak)=relAmplitude_fat(index,indexPeak)/sum(relAmplitude_fat(index,:));
    end

end

%% Non linear least squares fit for each peak
[ T1_TI, r_squared ] = STEAM_TI_T1( TI, relAmplitude, freqPeaks, tr);


%% Two pool relaxation model fit

% To use T1 measurements in the safflower oil emulsion, load these mat
% files:
if safflowerOilEmulsion==1
    load('peaksmicrolipid_20200725_2_noAp.mat');load('peaksmicrolipid_20200725_1_noAp.mat');
    FFw=[6.30,12.67,25.60,38.81,45.52,52.30,6.30,12.67,25.60,38.81,45.52,52.30]/100;
    R1w=[1./(peaksmicrolipid_20200725_1_noAp.T1(9,2:7)./1000),1./(peaksmicrolipid_20200725_2_noAp.T1(9,2:7)./1000)];
    WaterPoint=[peaksmicrolipid_20200725_1_noAp.T1(9,1)./1000,peaksmicrolipid_20200725_1_noAp.T1(9,1)./1000];

    % Define the maximum fat content and the number of fat resonances. Values for the phantom used in the publication are specified below
    maxFF=0.51;
    maxResonance=6;
end


% To use T1 measurements in the dairy cream, load these mat
% files:   
if dairyCream==1
     load('peaksdairycream_20200725_2_Final.mat');load('peaksdairycream_20200725_1_Final.mat');
     FFw=[4.4, 8.79, 17.55, 26.29, 30.65, 35, 8.79, 17.55, 26.29, 30.65, 35]/100;
     R1w=[1./(peaksdairycream_20210403_1_noAp.T1(9,:)./1000),1./(peaksdairycream_20210403_2_noAp.T1(9,2:end)./1000)];
     WaterPoint=[peaksdairycream_20200725_1_noAp.T1(9,1)./1000,peaksdairycream_20200725_2_noAp.T1(9,1)./1000];

     % Define the maximum fat content and the number of fat resonances. Values for the phantom used in the publication are specified below
     maxFF=0.36;
     maxResonance=5;
end


r1_gd=4.51; % (s-1 L mmol-1)
volGd=0.16; % (ml)
Gd_concentration=31.25; % (mM = mmol/L)

variable_Gd=[(volGd*Gd_concentration)./((1-FFw)*50)];

subplot = @(m,n,p) subtightplot (m, n, p, [0.1 0.1], [0.1 0.1], [0.1 0.1]);
clear x2 resnorm residuals residualsConst2 residualsVar2 x2Var
figure()
for indexPeak=1:maxResonance
        

    %     Safflower oil emulsion        
    if safflowerOilEmulsion==1 
        if indexPeak==5
                FF=[6.30,12.67,38.81,45.52,52.30,6.30,12.67,25.60,38.81,45.52,52.30]/100;
                R1f=[1./(peaksmicrolipid_20200725_1_noAp.T1(indexPeak,2:3)./1000),1./(peaksmicrolipid_20200725_1_noAp.T1(indexPeak,5:7)./1000),1./(peaksmicrolipid_20200725_2_noAp.T1(indexPeak,2:7)./1000)];
        else
            FF=[6.30,12.67,25.60,38.81,45.52,52.30,6.30,12.67,25.60,38.81,45.52,52.30]/100;
            R1f=[1./(peaksmicrolipid_20200725_1_noAp.T1(indexPeak,2:7)./1000),1./(peaksmicrolipid_20200725_2_noAp.T1(indexPeak,2:7)./1000)];
        end
    end


    %     Dairy cream  
    if dairyCream==1   
        FF=[4.4, 8.79, 17.55, 26.29, 30.65, 35, 8.79, 17.55, 26.29, 30.65, 35]/100;
        R1f=[1./(peaksdairycream_20210403_1_noAp.T1(indexPeak,:)./1000),1./(peaksdairycream_20210403_2_noAp.T1(indexPeak,2:end)./1000)];
    end

        variable_Gd_f=[(volGd*Gd_concentration)./((1-FF)*50)];

        R1_modeling_withGdVar=@(y) [R1w-(y(1).*(1-FFw).^0+y(2).*FFw.^y(3)+r1_gd*variable_Gd.*(1-FFw.^y(3))),R1f-(y(2).*FF.^y(4)+y(1).*(1-FF).^y(5)+r1_gd*variable_Gd_f.*(1-FF).^y(5))]; % new model

        x0=ones(1,5);
        lb=ones(1,5);
        ub=ones(1,5);

        x0(1,1)=1;
        x0(1,2)=4;
        x0(1,3)=1;
        x0(1,4)=1;
        x0(1,5)=1;

        lb(1,1)=1/6;
        lb(1,2)=1/3;
        lb(1,3)=0;
        lb(1,4)=0;
        lb(1,5)=0;

        ub(1,1)=1/0.4;
        ub(1,2)=1/0.005;
        ub(1,3)=20;
        ub(1,4)=20;
        ub(1,5)=20;

        optionFit = optimoptions(@lsqnonlin,'Algorithm','trust-region-reflective','FunctionTolerance',1e-12,'StepTolerance',1e-12,'MaxIterations',5000);

        problem = createOptimProblem('lsqnonlin','x0',x0,'objective',R1_modeling_withGdVar,...
        'lb',lb,'ub',ub,'options',optionFit);
    
        ms=MultiStart('StartPointsToRun','bounds');
    
        [x2Var(indexPeak,:),errormulti(indexPeak,:)] = run(ms,problem,100);

        residualsVar2=[R1w-(x2Var(indexPeak,1).*(1-FFw).^0+x2Var(indexPeak,2).*FFw.^x2Var(indexPeak,3)+r1_gd*variable_Gd.*(1-FFw.^x2Var(indexPeak,3))),R1f-(x2Var(indexPeak,2).*FF.^x2Var(indexPeak,4)+x2Var(indexPeak,1).*(1-FF).^x2Var(indexPeak,5)+r1_gd*variable_Gd_f.*(1-FF).^x2Var(indexPeak,5))]; % new model

        SSE=sum(residualsVar2(1:(size(FFw,2))).^2);
        y_mean=mean(R1w);
        SST=sum((R1w-y_mean).^2);
        r_squaredW_var2(indexPeak)=1-SSE/SST;
        
        SSE=sum(residualsVar2((size(FFw,2)+1):end).^2);
        y_mean=mean(R1f);
        SST=sum((R1f-y_mean).^2);
        r_squaredF_var2(indexPeak)=1-SSE/SST;
        
        
        % Plot the results safflower oil emulsion
        if safflowerOilEmulsion==1 
            maxFF=0.54;
            str=sprintf('Fat resonance: %0.1f ppm',round(freqPeaks(indexPeak),1));
            subplot(3,2,indexPeak);hold on;
            plot(FF(1:floor(size(FF,2)/2))*100,1./R1f(1:floor(size(FF,2)/2))*1000,' rs','markersize',10,'markerfacecolor','r','linewidth',2);plot(FFw(1:(size(FFw,2)/2))*100,1./R1w(1:(size(FFw,2)/2))*1000,' bd','markersize',10,'markerfacecolor','b','linewidth',2);plot(FFw((size(FFw,2)/2+1):end)*100,1./R1w((size(FFw,2)/2+1):end)*1000,' bd','markersize',10,'linewidth',2);plot(FF((floor(size(FF,2)/2)+1):end)*100,1./R1f((floor(size(FF,2)/2)+1):end)*1000,' rs','markersize',10,'linewidth',2);...
            plot(0,WaterPoint(1)*1000,' kd','markersize',10,'markerfacecolor','k','linewidth',2);plot(0,WaterPoint(2)*1000,' kd','markersize',10,'linewidth',2);...
            plot([0:maxFF*100],(1./(x2Var(indexPeak,2).*[0:0.01:maxFF].^x2Var(indexPeak,4)+x2Var(indexPeak,1).*(1-[0:0.01:maxFF]).^1+r1_gd*((volGd*Gd_concentration)./((1-[0:0.01:maxFF])*50)).*(1-[0:0.01:maxFF]).^1))*1000,':r','linewidth',2);...
            plot([0:maxFF*100],(1./(x2Var(indexPeak,1).*(1-[0:0.01:maxFF]).^0+x2Var(indexPeak,2).*[0:0.01:maxFF].^x2Var(indexPeak,3)+r1_gd*((volGd*Gd_concentration)./((1-[0:0.01:maxFF])*50)).*(1-[0:0.01:maxFF].^x2Var(indexPeak,3))))*1000,':b','linewidth',2);
            plot([0:maxFF*100],(1./((r1_gd*(volGd*Gd_concentration)./((1-[0:0.01:maxFF])*50))+0.289))*1000,'k--','linewidth',2)
            set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');xlabel('Fat fraction [%]');ylabel('T_1 [ms]');...
            legend(str,'Water');axis([0 maxFF*100 150 1400]);grid on; grid minor;
        end


        if dairyCream==1
        % Plot the results dairy cream
            maxFF=0.36;
            str=sprintf('Fat resonance: %0.1f ppm',round(freqPeaks(indexPeak),1));
            subplot(3,2,indexPeak);hold on;
            plot(FF(1:ceil(size(FF,2)/2))*100,1./R1f(1:ceil(size(FF,2)/2))*1000,' rs','markersize',10,'markerfacecolor','r','linewidth',2);plot(FFw(1:ceil(size(FFw,2)/2))*100,1./R1w(1:ceil(size(FFw,2)/2))*1000,' bd','markersize',10,'markerfacecolor','b','linewidth',2);plot(FFw(ceil(size(FFw,2)/2+1):end)*100,1./R1w(ceil(size(FFw,2)/2+1):end)*1000,' bd','markersize',10,'linewidth',2);plot(FF((ceil(size(FF,2)/2)+1):end)*100,1./R1f((ceil(size(FF,2)/2)+1):end)*1000,' rs','markersize',10,'linewidth',2);...
            plot(0,WaterPoint(1)*1000,' kd','markersize',10,'markerfacecolor','k','linewidth',2);plot(0,WaterPoint(2)*1000,' kd','markersize',10,'linewidth',2);...
            plot([0:maxFF*100],(1./(x2Var(indexPeak,2).*[0:0.01:maxFF].^x2Var(indexPeak,4)+x2Var(indexPeak,1).*(1-[0:0.01:maxFF]).^x2Var(indexPeak,5)+r1_gd*((volGd*Gd_concentration)./((1-[0:0.01:maxFF])*50)).*(1-[0:0.01:maxFF]).^x2Var(indexPeak,5)))*1000,':r','linewidth',2);...
            plot([0:maxFF*100],(1./(x2Var(indexPeak,1).*(1-[0:0.01:maxFF]).^0+x2Var(indexPeak,2).*[0:0.01:maxFF].^x2Var(indexPeak,3)+r1_gd*((volGd*Gd_concentration)./((1-[0:0.01:maxFF])*50)).*(1-[0:0.01:maxFF].^x2Var(indexPeak,3))))*1000,':b','linewidth',2);
            plot([0:maxFF*100],(1./((r1_gd*(volGd*Gd_concentration)./((1-[0:0.01:maxFF])*50))+0.289))*1000,'k--','linewidth',2)
            set(gca,'fontsize',20','fontweight','bold');set(gcf,'color','w');xlabel('Fat fraction [%]');ylabel('T_1 [ms]');...
            legend(str,'Water');axis([0 maxFF*100 150 1400]);grid on; grid minor;
        end

end


